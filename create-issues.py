#!/usr/bin/env python
import webbrowser as wb

import json
from restkit import Resource, BasicAuth

'''
SUPER ALPHA 0.0000000001

you should have a file in your cwd called 'tickets' in the following format:

    Task    Shoop the whoop
    Spike   Get in the chopper
    Task    Open the pod bay doors

This will create three tickets in INF: two tasks and a spike (wow!) and open their URLs in your browser

'''


def create_task(server_base_url, user, password, project, task_summary, issue_type):
    auth = BasicAuth(user, password)

    resource_name = "issue"
    complete_url = "%s/rest/api/latest/%s/" % (server_base_url, resource_name)
    resource = Resource(complete_url, filters=[auth])

    try:
        data = {
            "fields": {
                "project": {
                    "key": project
                },
                "summary": task_summary,
                "issuetype": {
                    "name": issue_type
                }
            }
        }
        response = resource.post(headers={'Content-Type': 'application/json'}, payload=json.dumps(data))
    except Exception, ex:
        print "EXCEPTION: %s " % ex.msg
        return None

    if response.status_int / 100 != 2:
        print "ERROR: status %s" % response.status_int
        return None

    issue = json.loads(response.body_string())

    return issue

if __name__ == '__main__':
    server_url = 'https://ensighten.atlassian.net'
    project = 'INF'

    username = 's.cunningham'
    password = "come on, you didn't really think I'd leave this here, did you?"

    lines = open('tickets').readlines()

    for line in lines:
        parts = line.split()
        issue_desc = ' '.join(parts[1:])
        issue_type = parts[0]

        issue = create_task(server_url, username, password, project, issue_desc, issue_type)
        issue_code = issue["key"]
        issue_url = "%s/browse/%s" % (server_url, issue_code)

        if (issue is not None):
            print issue
            wb.open(issue_url)
        else:
            print 'ERROR: Issue could not be created!'
            print issue_desc
